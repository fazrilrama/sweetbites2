<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductGallery;
use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data =  Product::with('galleries')->get();
        return view('backend.product.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //untuk validasi form
        $validated = $request->validate([
            'ProductName' => 'required',
            'Price' => 'required|integer',
            'Categories' => 'required',
            'Weight' => 'required|integer',
            'Description' => 'required',
            'ThumbnailPhoto.*' => 'required|mimes:jpg,jpeg,png',
        ]);

        // $data =  $request->all();
        // $data['ThumbnailPhoto'] = $request->file('ThumbnailPhoto')[0]->store('assets/product', 'public',$request->file('ThumbnailPhoto')[0]->GetClientOriginalName());
        // $image = $request->file('ThumbnailPhoto');
        // $fileName = $image->getClientOriginalName();
        // $request->file('ThumbnailPhoto')->move(\public_path('storage'), $fileName);
        // $data = $request->all();
        // $product =  Product::create($data);

                   
        $filename = str_replace(' ','-',$request->file('ThumbnailPhoto')->getClientOriginalName());
        $request->file('ThumbnailPhoto')->move(\public_path('foto'), $filename);
        $product =  Product::create([
            'ProductName' => $request->ProductName,
            'Price' => $request->Price,
            'Weight' => $request->Weight,
            'Categories' => $request->Categories,
            'Description' => $request->Description,
            'ThumbnailPhoto' => $filename,
        ]);

        $gallery = [
            'Products_id' => $product -> id,
            'Photos' =>  $filename,
        ];
        ProductGallery::create($gallery);
        return redirect('/product')->with('message', 'Data telah ditambahkan');



        // $gallery = [
        //     'Products_id' => $product -> id,
        //     'Photos' =>  $request->file('ThumbnailPhoto')->store('assets/product', 'public'),
        // ];

        // ProductGallery::create($gallery);

        // foreach($request->file('ThumbnailPhoto') as $foto){
        //     $gallery = [
        //         'Products_id' => $product -> id,
        //         'Photos' =>  $foto->store('assets/product', 'public'),
        //     ];
    
        //     ProductGallery::create($gallery);
        // }

        // return redirect('/product')->with('message', 'Data telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Product::findOrFail($id);
        return view('backend.product.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //contoh jika error messagenya diubah
        $validated = $request->validate([
            //nama input sama dengan yang ada di view
            'ProductName' => 'required',
            'Price' => 'required|integer',
            'Categories' => 'required',
            'Weight' => 'required|integer',
            'Description' => 'required',
            'ThumbnailPhoto' => 'mimes:jpg,jpeg,png',
        ],[
            // 'namainput.rule' => 'custom error message'
            'Description.required' => 'Deskripsi belum diisi',
            'ThumbnailPhoto.mimes' => 'File bukan foto'
        ]);

        if(empty($request->ThumbnailPhoto)){
            $item = Product::findOrFail($id);
            $data = $request->all();
            $insert =  $item->update($data);
        }
        else{
            $item = Product::findOrFail($id);
            $data = $request->all();
            // $data['ThumbnailPhoto'] = $request->file('ThumbnailPhoto')->store('assets/product', 'public',$request->file('ThumbnailPhoto')->GetClientOriginalName());
            $data['ThumbnailPhoto'] = str_replace(' ','-',$request->file('ThumbnailPhoto')->getClientOriginalName());
            $request->file('ThumbnailPhoto')->move(\public_path('foto'), $data['ThumbnailPhoto']);
    
            //ubah thumbnail pada gallery
            ProductGallery::where('Products_id',$id)->where('Photos',$item->ThumbnailPhoto)->update(['Photos'=>$data['ThumbnailPhoto']]);

            $insert =  $item -> update($data);
        }
     

        return redirect('/product')->with('message', 'Data telah diupdate');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //bug cart
        Cart::with('Product')->where('Products_id',$id)->delete();
        ProductGallery::with('Product')->where('Products_id',$id)->delete();
        Product::findOrFail($id)->delete();
        
        return redirect('/product')->with('message', 'Data telah dihapus');
    }
}
