<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductGallery;
use App\Models\Transaction;

class DashboardController extends Controller
{
    public function index(){
        $customer = ProductGallery::all()->count();
        $transaction = Transaction::all()->where('transaction_status','SUCCESS')->count();
        $pending = Transaction::all()->where('transaction_status','PENDING')->count();
        $januari = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '01')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        $februari = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '02')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        $maret = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '03')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        $april = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '04')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        $mei = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '05')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        $juni = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '06')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        $juli = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '07')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        $agustus = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '08')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        $september = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '09')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        $oktober = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '10')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        $november = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '11')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        $desember = Transaction::select(\DB::raw("COUNT(*) as count"))->whereMonth('created_at', '12')->groupBy(\DB::raw("Month(created_at)"))->pluck('count');
        

        // dd($januari);
        // $user = Auth
        return view('backend.dashboard',compact('customer','transaction','pending', 'januari', 'februari', 'maret', 'april', 'mei', 'juni', 'juli', 'agustus', 'september', 'oktober', 'november', 'desember'));
    }
}
