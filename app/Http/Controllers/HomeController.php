<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'id_transaksi' => 'required',
            'file' => 'required',
            'file.*' => 'mimes:doc,docx,PDF,pdf,jpg,jpeg,png|max:2000'
        ]);
        if ($request->hasfile('file')) {            
            $filename = str_replace(' ','-',$request->file('file')->getClientOriginalName());
            $request->file('file')->move(\storage_path('bukti'), $filename);
            Pembayaran::insert([                        
                'id_transaksi' => $request->id_transaksi,
                'user_id' => $request->user_id,
                'image' =>$filename,
            ]);
            return redirect('/orderhistory');
        }else{
            echo'Gagal';
        }
    }

    public function uploadBukti(Request $request, $id)
    {
        $request->validate([
            'file' => 'required',
            'file.*' => 'mimes:doc,docx,PDF,pdf,jpg,jpeg,png|max:2000'
        ]);
        $upload = $request->hasfile('file');
        if($upload == false){
            echo "Gagal";
        } else {
            $fileName = str_replace(' ','-',$request->file('file')->getClientOriginalName());
            $upload->move(\storage_path('bukti'), $fileName);
            $update = Transaction::findOrFail($id);
            $update->bukti = $fileName;
            $update->save();
            return redirect('order-history');
        }
    }

    
}
