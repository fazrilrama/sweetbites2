<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\TransactionDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


use Exception;


class CheckoutController extends Controller
{

    public function checkoutdata(Request $request)
    {
        $validated = $request->validate([
            'Quantity' => 'required|integer',
            'name'=> 'required',
            'email' => 'required|email',
            'address_one' => 'required',
            'address_two' => 'required',
            'province' => 'required',
            'city' => 'required',
            'zip_code' => 'required|regex:/^[0-9]*$/',
            'phone' => 'required|regex:/^[0-9]*$/',
        ]);


        DB::table('carts')->update([
            'Quantity' => $request -> Quantity,
        ]);

        $user = Auth::user();
        $request -> all();
        $name = $request -> name;
        $totalprice = $request -> totalprice;
        $email = $request -> email;
        $address_one = $request -> address_one;
        $address_two = $request -> address_two;
        $province = $request -> province;
        $city = $request -> city;
        $zip_code = $request -> zip_code;
        $phone = $request -> phone;
        $Quantity = $request -> Quantity;

        $carts = Cart::with(['product','user'])
        ->where('users_id', Auth::user()->id)
        ->get();
        $user = Auth::user();
        return view('frontend.paymentorder',compact('name','Quantity','totalprice','email','address_one','address_two','province','city','zip_code','phone','carts', 'user'));
    }

    public function process(Request $request)
    {
        $user = Auth::user();
        $code = 'STORE-' . mt_rand(0000,9999);

        $carts = Cart::with(['product','user'])->where('users_id', Auth::user()->id)->get();

        $transaction = Transaction::create([
            'users_id' => Auth::user()->id,
            'total_price' => $request->total_price,
            'transaction_status' => 'PENDING',
            'name' => $request -> name,
            'email' => $request -> email,
            'address_one' =>  $request -> address_one,
            'address_two' =>  $request -> address_two,
            'province' => $request -> province,
            'city' => $request -> city,
            'zip_code' => $request -> zip_code,
            'phone' => $request -> phone,
            'code' => $code
        ]);

        foreach ($carts as $cart) {
            TransactionDetail::create([
                'transactions_id' => $transaction->id,
                'products_id' => $cart->product->id,
                'price' => $cart->product->Price,
                'quantity' => $cart-> Quantity,
            ]);
        }
        Cart::with(['product','user'])
        ->where('users_id', Auth::user()->id)
        ->delete();

        try {
            // Ambil halaman payment midtrans
            // $paymentUrl = Snap::createTransaction($midtrans)->redirect_url;
            // Redirect ke halaman midtrans
            return redirect('/order-history')->with('message', 'Pesanan telah dibuat, silahkan melakukan pembayaran');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
 
    }

    public function order()
    {
        // $carts = Cart::with(['product.galleries', 'user'])
        // ->where('users_id', Auth::user()->id)
        // ->get();

       $transaction = Transaction::orderBy('id','DESC')
       ->where('users_id',Auth::user()->id)->first();
       
        // $transactiondetail = TransactionDetail::with('product.galleries','transaction')->where('id',$transaction->id)->get();

        //    return view('frontend.paymentorder',compact('carts','transaction','transactiondetail'));
    }

    public function callback(Request $request)
    {

        $notification = new Notification();

        $status = $notification->transaction_status;
        $type = $notification->payment_type;
        $fraud = $notification->fraud_status;
        $order_id = $notification->order_id;

        $transaction = Transaction::findOrFail($order_id);

        if($status == 'capture'){
            if($type = 'credit_card'){
                if($fraud == 'challenge'){
                    $transaction->status = 'PENDING';
                }
                else{
                    $transaction->status = 'SUCCESS';
                }
            }
        }

        else if($status == 'settlement'){
            $transaction->status = 'SUCCESS'; 
        }
        else if($status == 'pending'){
            $transaction->status = 'PENDING';
        }
        else if($status == 'deny'){
            $transaction->status = 'CANCELLED';
        }
        else if($status == 'expire'){
            $transaction->status = 'CANCELLED';
        }
        else if($status == 'cancel'){
            $transaction->status = 'CANCELLED';
        }

        $transaction->save();
    }
}
