<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Review;
use App\Models\Product;
use App\Models\Pembayaran;
use App\Models\TransactionDetail;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class LandingPageController extends Controller
{

    public function index(){
        $user = Auth::user();
        $data  =  Product::with('galleries')->get()->take(8);
        $suggestproduct  =  Product::with('galleries')->get()->take(3);
        // $foto = Storage::url('app/public/assets/product/1.jpg');
        // dd($foto);
        // $suggestproduct = Product::get();
        //dd($suggestproduct);

       return view('frontend.LandingPage', compact('data','suggestproduct', 'user'));
    }
    public function detail($id){
        $user = Auth::user();
        $review = Review::with('product','user')->where('Products_id',$id)->get();
        $data =  Product::with('galleries')->findOrFail($id);

        return view('frontend.details',compact('data','review', 'user'));
    }

    public function add(Request $request,$id){
        $data = [
            'products_id' => $id,
            'users_id' => Auth::user()->id
        ];
        $user = Auth::user();

        Cart::create($data);

        return redirect()->route('cart.index', compact('user'));
    }

    public function categories(){
        $user = Auth::user();
        $data  =  Product::with('galleries')->get();
        $suggestproduct  =  Product::with('galleries')->get()->take(3);
        return view('frontend.categories',compact('data', 'suggestproduct', 'user'));
    }

    public function addreview(Request $request,$id)
    {
        $data =  $request -> all();
        $data['users_id'] = Auth::user()->id;
        $data['Products_id'] = $id;

        Review::create($data);
        return redirect()->route('detail',$id);

    }

    public function orderhistory()
    {
        $user = Auth::user();
        $history = Transaction::with('transactiondetail.product.galleries')->where('users_id',Auth::user()->id)->get();
        // $id = Auth::user()->id;
        // $history = Transaction::where('users_id', $id)->get();
        // dd($history);
        return view('frontend.orderhistory',compact('history', 'user'));
    }

    public function ordershow(Request $request, $id)
    {   
        $history = Transaction::with('transactiondetail.product.galleries')->where('id',$id)->first();
        $items = TransactionDetail::with('transaction','product.galleries')->where('transactions_id',$id)->get();
        $user = Auth::user();
        return view('frontend.showhistory',compact('history','items', 'user'));
    }
    
    public function aboutme()
    {
        $user = Auth::user();
        return view('frontend.about', compact('user'));
    }

    // Upload File
    public function uploadBukti(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'file' => 'required',
            'file.*' => 'mimes:doc,docx,PDF,pdf,jpg,jpeg,png|max:2000'
        ]);
        $id = $request->id;
        $upload = $request->hasfile('file');
        if($upload == false){
            echo "Gagal";
        } else {
            $fileName = str_replace(' ','-',$request->file('file')->getClientOriginalName());
            $upload->move(\storage_path('bukti'), $fileName);
            $update = Transaction::findOrFail($id);
            $update->bukti = $fileName;
            $update->save();
            return redirect('order-history');
        }
    }

    public function addBukti(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'id_transaksi' => 'required',
            'file' => 'required',
            'file.*' => 'mimes:doc,docx,PDF,pdf,jpg,jpeg,png|max:2000'
        ]);
        if ($request->hasfile('file')) {            
            $filename = str_replace(' ','-',$request->file('file')->getClientOriginalName());
            $request->file('file')->move(\storage_path('bukti'), $filename);
            Pembayaran::create([
                'id_transaksi' => $request->id_transaksi,
                'user_id' => $request->user_id,                        
                'image' =>$filename
            ]);
            return redirect('/orderhistory');
        }else{
            echo'Gagal';
        }
    }

    public function Example() {
        $data = [
            'title' => 'Example',
            'judul' => 'Example',
        ];

        return view('example');
    }
}
