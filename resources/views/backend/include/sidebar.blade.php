   <div class="border-right" id="sidebar-wrapper">
       <div class="sidebar-heading text-center">
           <img src="{{ asset('frontend/images/logo1.png')}}" alt="" class="my-4" />
       </div>
       <div class="list-group list-group-flush">
           <a href="{{ route('dashboard') }}" class="list-group-item list-group-item-action active">Dashboard Admin</a>
           <a href="{{ route('product.index') }}" class="list-group-item list-group-item-action">My Menu</a>
           <a href="{{ route('product-galleries.index') }}" class="list-group-item list-group-item-action">Menu Galleries</a>
           <a href="{{ route('transaction.index') }}" class="list-group-item list-group-item-action">Transactions</a>
           {{-- <a href="{{ url('laporan') }}" class="list-group-item list-group-item-action">Laporan</a> --}}
       </div>
   </div>
