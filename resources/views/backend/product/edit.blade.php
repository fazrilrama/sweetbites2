@extends('backend.include.app')

@section('content')

    <div class="section-content section-dashboard-home">
        <div class="container-fluid">
            <div class="dashboard-heading">
                <h2 class="dashboard-title">Add New Menu</h2>
                <p class="dashboard-subtitle">
                    Create my own menu
                </p>
            </div>
            <div class="dashboard-content">
                <div class="row">
                    <div class="col-12">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ route('product.update',$data -> id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Menu Name</label>
                                                <input type="text" class="form-control" id="name" aria-describedby="name"
                                                    name="ProductName" value="{{ $data -> ProductName}}" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="price">Price</label>
                                                <input type="number" class="form-control" id="price"
                                                    aria-describedby="price" name="Price" value="{{$data -> Price}}" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="categories">Category</label>
                                                <select class="form-control" id="category" name="Categories">
                                                    <option>{{$data -> Categories}}</option>
                                                    @if ($data -> Categories == 'Dessert Cup')
                                                    <option>Baked Brownies</option>
                                                    <option>Dessert Box</option>
                                                    @elseif($data -> Categories == 'Baked Brownies') 
                                                    <option>Dessert Cup</option>
                                                    <option>Dessert Box</option>
                                                    @else
                                                    <option>Dessert Cup</option>
                                                    <option>Baked Brownies</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="categories">Weight (Gr) </label>
                                                <input type="number" class="form-control" id="price"
                                                    aria-describedby="price" name="Weight" value="{{ $data -> Weight}}" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="description">Descriptions</label>
                                                <textarea name="Description" id="" cols="30" rows="4"
                                                    class="form-control" required> {{$data -> Description }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="thumbnails">Thumbnails</label>
                                                <input type="file" multiple class="form-control pt-1" id="thumbnails"
                                                    aria-describedby="thumbnails" name="ThumbnailPhoto" accept=".png, .jpeg, .jpg"/>
                                                <small class="text-muted">
                                                    Kamu dapat memilih Gambar untuk thumbnail
                                                </small>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success btn-block px-5">
                                                    Save Now
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
