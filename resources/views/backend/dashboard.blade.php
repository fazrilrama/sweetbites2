@extends('backend.include.appGrafik')

@section('content')

    <div class="section-content section-dashboard-home">
        <div class="container-fluid">
            <div class="dashboard-heading">
                <h2 class="dashboard-title">Dashboard Admin</h2>
                <p class="dashboard-subtitle">
                    Look what you have made today!
                </p>
            </div>
            <div class="dashboard-content">
            @if (session('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Selamat!</strong> {{ session('message') }}
            </div>
            @endif
                <div class="row">
                    <div class="col-md-4">
                        <div class="card mb-2">
                            <div class="card-body">
                                <div class="dashboard-card-title">
                                    Total Customer
                                </div>
                                <div class="dashboard-card-subtitle">
                                   {{$customer}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card mb-2">
                            <div class="card-body">
                                <div class="dashboard-card-title">
                                    Success
                                </div>
                                <div class="dashboard-card-subtitle">
                                    {{$transaction}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card mb-2">
                            <div class="card-body">
                                <div class="dashboard-card-title">
                                    Pending
                                </div>
                                <div class="dashboard-card-subtitle">
                                    {{$pending}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <canvas id="myChart" width="80" height="31" class="p-2"></canvas>
            </div>
        </div>
    </div>

@endsection
