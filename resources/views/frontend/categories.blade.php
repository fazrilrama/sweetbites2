@extends('frontend.include.app')

@section('content')

    <!-- Page Content -->
    <div class="page-content page-categories">
        <section class="store-trend-categories">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h5 class="text-center"> Category</h5>
                        <img src="{{ asset('frontend/images/logo1.png') }}" alt="">
                    </div>
                </div>
                @if (session('message'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Selamat!</strong> {{ session('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

                <div class="row mt-4">
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="200">
                        <div class="component-categories d-block">
                            <div class="categories-image">
                                <img src="{{ asset('frontend/images/dessert.jpg') }}" alt="Gadgets Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Dessert Box
                            </p>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="600">
                        <div class="component-categories d-block">
                            <div class="categories-image">
                                <img src="{{ asset('frontend/images/milo.jpg') }}" alt="Furniture Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Dessert Cup
                            </p>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="400">
                        <div class="component-categories d-block">
                            <div class="categories-image">
                                <img src="{{ asset('frontend/images/brownies.jpg') }}" alt="Makeup Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Baked Brownies
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @if($user == true)
            <section class="store-trend-categories">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center" data-aos="fade-up">
                            <h5 class="text-center"> Suggest Menu</h5>
                            <img src="{{ asset('frontend/images/logo2.png') }}" alt="">
                        </div>
                    </div>
                    <div class="row mt-4 ">
                        @php $incrementProduct = 0 @endphp
                        @foreach ($suggestproduct as $item)
                        <div class="col-6 col-md-3 col-lg-4 pt-3" data-aos="fade-up" data-aos-delay="{{ $incrementProduct += 100 }}">
                            <a class=" d-block" style="text-decoration:none" href="{{ route('detail', $item->id) }}">
                                <p class="categories-text ">
                                    <h4 class="text-center"> {{ $item->ProductName }} </h4>
                                </p>
                                <div class="categories-image">
                                    <img src="{{ asset('foto/' . $item->galleries->first()->Photos) }}" alt="Gadgets Categories" class="w-100" height="217px" />


                                    {{-- <img src="https://wedding.skripti.xyz//thome/img/2.jpg" alt="Gadgets Categories" class="w-100" height="217px" /> --}}
                                </div>
                                <p class="categories-text ">
                                    <h6 class="text-center"> {{ $item->Categories }} </h6>
                                </p>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
        @else


        @endif


        <section class="store-new-products mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h5 class="text-center">Our Menu </h5>
                        <img src="{{ asset('frontend/images/logo2.png') }}" width="80px" height="57px" alt="">
                    </div>
                </div>
                <div class="row mt-5">
                    @php $incrementProduct = 0 @endphp
                    @foreach ($data as $item)
                        <div class="col-6 col-md-4 col-lg-3" data-aos="fade-right"
                            data-aos-delay="{{ $incrementProduct += 100 }}">
                            <a class="component-products d-block" href="{{ route('detail', $item->id) }}">
                                <div class="products-thumbnail">
                                    <div class="products-image"
                                        style="background-image: url('{{ asset('foto/' . $item->galleries->first()->Photos) }}');">
                                    </div>
                                </div>
                                <div class="products-text">
                                    {{ $item->ProductName }}
                                </div>
                                <div class="products-price">
                                    Rp {{ number_format($item->Price)  }}
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>


@endsection
