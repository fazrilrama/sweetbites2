@extends('frontend.include.app')

@section('content')

    <!-- Page Content -->
    <div class="page-content page-categories">
        <section class="store-trend-categories">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h5 class="text-center"> About Us </h5>
                        <img src="{{ asset('frontend/images/logo1.png') }}" alt="">
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12  col-sm-12 col-lg-6" data-aos="fade-up" data-aos-delay="600">
                        <div class="component-categories d-block">
                            <div class="categories-image text-center" style="height: 455px">
                               <h5> Sweetbites by Caca merupakan sebuah <br /> home industry yang berlokasi di Jagakarsa dan dikelola langsung oleh seorang mahasiswi dari Universitas Gunadarma. Pada awalnya owner hanya menjual satu macam saja yaitu Regal Dessert Box. Namun karena banyaknya permintaan dari berbagai belah pihak kini Sweetbites By Caca juga menjual berbagai menu lainnya. <br /> <br /> So, what do you waiting for? <br /> Let's buy some! ^^ </h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-6" data-aos="fade-up" data-aos-delay="400">
                        <div class="component-categories d-block">
                            <div class="categories-image">
                                <img src="{{ asset('frontend/images/dessert.jpg') }}" alt="Gadgets Categories"
                                    class="w-100" height="400px" />
                            </div>
                            <p class="categories-text">
                                Regal Dessert Box
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('fixed','fixed-bottom')
    

    
