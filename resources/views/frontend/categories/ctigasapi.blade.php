@extends('frontend.include.app')

@section('content')

    <!-- Page Content -->
    <div class="page-content page-categories">
        <section class="store-trend-categories">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h5 class="text-center"> Category</h5>
                        <img src="{{ asset('frontend/images/logo1.png') }}" alt="">
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="100">
                        <a class="component-categories d-block" href="{{ route('ctigasapi') }}">
                            <div class="categories-image">
                                <img src="{{ asset('frontend/images/dessert.jpg') }}" alt="Gadgets Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Dessert Box
                            </p>
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="200">
                        <a class="component-categories d-block" href="{{ route('ctayam') }}">
                            <div class="categories-image">
                                <img src="{{ asset('frontend/images/milo.jpg') }}" alt="Furniture Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Dessert Cup
                            </p>
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="300">
                        <a class="component-categories d-block" href="{{ route('cthas') }}">
                            <div class="categories-image">
                                <img src="{{ asset('frontend/images/brownies.jpg') }}" alt="Makeup Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Baked Brownies
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="store-new-products mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h5 class="text-center"> Our Dessert Box </h5>
                        <img src="{{ asset('frontend/images/logo2.png') }}" width="80px" height="57px" alt="">
                    </div>
                </div>
                <div class="row mt-5">
                    @php $incrementProduct = 0 @endphp
                    @foreach ($data as $item)
                        <div class="col-6 col-md-4 col-lg-3" data-aos="fade-right"
                            data-aos-delay="{{ $incrementProduct += 100 }}">
                            <a class="component-products d-block" href="{{ route('detail', $item->id) }}">
                                <div class="products-thumbnail">
                                    <div class="products-image"
                                        style="background-image: url('{{ Storage::url($item->galleries->first()->Photos) }}');">
                                    </div>
                                </div>
                                <div class="products-text">
                                    {{ $item->ProductName }}
                                </div>
                                <div class="products-price">
                                    Rp {{ number_format($item->Price)  }}
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>


@endsection
