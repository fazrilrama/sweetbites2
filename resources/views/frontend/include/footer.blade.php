   <footer class="@yield('fixed-bottom')">
       <div class="container">
           <div class="row">
               <div class="col-12 text-center">
                   <p class="pt-4 pb-2">
                       2021 Copyright Sweetbites By Caca Web Store. All Rights Reserved.
                   </p>
               </div>
           </div>
       </div>
   </footer>