@extends('frontend.include.app')

@section('content')
    <!-- Page Content -->
    <div class="page-content page-categories">  
    <h3 class="container-fluid mr-2 text-center"> Order History </h3>
        <div class="container">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Selamat!</strong> {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
            <div class="card">
                <div class="col-12 mt-2">
                        <div class="card-body">
                            <table class="table">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col" class="pt-4 pb-4">Code Order</th>
                                    <th scope="col" class="pt-4 pb-4">Total Price</th>
                                    <th scope="col" class="pt-4 pb-4">Name</th>
                                    <th scope="col" class="pt-4 pb-4">Status</th>
                                    <th scope="col" class="pt-4 pb-4">Payment</th>
                                    <th scope="col" class="pt-4 pb-4">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($history as $item)
                                    <tr>
                                        <td>
                                        <h4> {{$item  -> code}} </h4>
                                        <p> {{ $item  -> created_at }}</p>
                                        </td>
                                        <td>
                                            {{ number_format($item->total_price)}}
                                        </td>
                                        <td>
                                            {{$item -> name}}
                                        </td>
                                        <td>
                                            {{$item->transaction_status}}
                                        </td>
                                        <td>
                                            {{-- <a href="https://wa.me/6288212987850?text=Isi Pesan" class="btn btn-success">Upload Bukti</a> --}}

                                            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#upload{{ $item->id }}">Upload Bukti</a>

                                            {{-- <a href="#" class="btn btn-success" data-toggle="modal" data-target="#uploads{{ $item->id }}">Upload Bukti</a> --}}
                                        </td>
                                        
                                        <td>
                                            <form action="{{ route('ordershow',$item  -> id)}}" method="POST" enctype="multipart/form-data" >
                                            @csrf
                                                <button type="submit" class="btn btn-primary"> Show </button>
                                            </form>
                                        </td>
                                    </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Upload Bukti -->
    @foreach($history as $low)
        <div class="modal fade" tabindex="-1" id="upload{{ $low->id }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Upload Bukti Pembayaran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">  
                        <div class="card bg-light mb-3" style="max-width: 30rem;">
                            <div class="card-header">Nomor Rekening : <strong>Mandiri</strong></div>
                            <div class="card-body">
                                <h5 class="card-title">A/N Salsabila Endang Saf</h5>
                                <p class="card-text">1270009890821.</p>
                            </div>
                        </div>
                        <div>
                            <h5>Keterangan :</h5>
                            <p class="text-danger">
                                -Upload bukti pembayaran lewat whatsapp <br>
                                -Tulisan Otomatis jangan ubah atau dihapus.
                            </p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a href="https://wa.me/6287881859294?text=Nama: {{ $item->name }}, kode transaksi: {{ $item->id }}, kode: {{ $item->code }} " class="btn btn-primary">Upload</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    @foreach($history as $low)
        <div class="modal fade" tabindex="-1" id="uploads{{ $low->id }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Upload Bukti Pembayaran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form action="/uploadBukti" method="POST" enctype="multipart/form-data">
                            @csrf
                                <div class="form-group">
                                    <label>Kode : <strong>{{ $low->code }} :</strong></label>
                                    <label>Upload Bukti :</label>
                                    <input type="file" class="form-control-file" name="file">
                                    <input type="number" class="form-control" name="id_transaksi" value="{{ $low->id }}" readonly>
                                    <input type="number" class="form-control" name="id_transaksi" value="{{ Auth::user()->id }}" readonly>
                                </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card bg-light mb-3" style="max-width: 30rem;">
                                <div class="card-header">Nomor Rekening : <strong>Mandiri</strong></div>
                                <div class="card-body">
                                    <h5 class="card-title">A/N Salsabila Endang Saf</h5>
                                    <p class="card-text">1270009890821.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach

@endsection

@section('fixed','fixed-bottom')
