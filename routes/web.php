<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductGalleriesController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\LaporanController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\LandingPageController::class, 'index'])->name('landingpage');
Route::get('/detail/{id}', [App\Http\Controllers\LandingPageController::class, 'detail'])->name('detail');
Route::post('/detail/{id}', [App\Http\Controllers\LandingPageController::class, 'add'])->name('detail-add');

Route::get('/categories', [App\Http\Controllers\LandingPageController::class, 'categories'])->name('categories');

Route::post('/addreview/{id}',[App\Http\Controllers\LandingPageController::class, 'addreview'])->name('addreview');
Route::get('/orderhistory',[App\Http\Controllers\LandingPageController::class, 'orderhistory'])->name('order-history');
Route::put('/uploadBukti', [App\Http\Controllers\LandingPageController::class, 'uploadBukti'])->name('uploadBukti');
Route::post('/addBukti', function(Request $request){
    $request->validate([
        'user_id' => 'required',
        'id_transaksi' => 'required',
        'file' => 'required',
        'file.*' => 'mimes:doc,docx,PDF,pdf,jpg,jpeg,png|max:2000'
    ]);
    if ($request->hasfile('file')) {            
        $filename = str_replace(' ','-',$request->file('file')->getClientOriginalName());
        $request->file('file')->move(\storage_path('bukti'), $filename);
        Pembayaran::insert([                        
            'id_transaksi' => $request->id_transaksi,
            'user_id' => $request->user_id,
            'image' =>$filename,
        ]);
        return redirect('/orderhistory');
    }else{
        echo'Gagal';
    }
})->name('addBukti');
Route::post('/show-order/{id}',[App\Http\Controllers\LandingPageController::class, 'ordershow'])->name('ordershow');
Route::get('/about-me',[App\Http\Controllers\LandingPageController::class, 'aboutme'])->name('aboutme');


Auth::routes();

Route::post('/home/upload', [HomeController::class, 'index'])->name('upload');

Route::middleware(['auth', 'admin'])
    ->group(function() {

Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

Route::resource('product', ProductController::class);
Route::resource('transaction', TransactionController::class);
Route::resource('product-galleries', ProductGalleriesController::class);
Route::get('laporan', [LaporanController::class, 'index']);

    });

Route::resource('cart', CartController::class);

Route::post('/checkout', [App\Http\Controllers\CheckoutController::class, 'process'])->name('checkout');
Route::post('/checkout/callback', [App\Http\Controllers\CheckoutController::class, 'callback'])->name('midtrans-callback');
Route::post('/order', [App\Http\Controllers\CheckoutController::class, 'order'])->name('order');

Route::post('/checkoutdata', [App\Http\Controllers\CheckoutController::class, 'checkoutdata'])->name('checkoutdata');

