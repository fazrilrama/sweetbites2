<?php $__env->startSection('content'); ?>
    <!-- Page Content -->
    <div class="page-content page-categories">  
    <h3 class="container-fluid mr-2 text-center"> Order History </h3>
        <div class="container">
        <?php if(session('message')): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Selamat!</strong> <?php echo e(session('message')); ?>

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
            <div class="card">
                <div class="col-12 mt-2">
                        <div class="card-body">
                            <table class="table">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col" class="pt-4 pb-4">Code Order</th>
                                    <th scope="col" class="pt-4 pb-4">Total Price</th>
                                    <th scope="col" class="pt-4 pb-4">Name</th>
                                    <th scope="col" class="pt-4 pb-4">Status</th>
                                    <th scope="col" class="pt-4 pb-4">Payment</th>
                                    <th scope="col" class="pt-4 pb-4">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>
                                        <h4> <?php echo e($item  -> code); ?> </h4>
                                        <p> <?php echo e($item  -> created_at); ?></p>
                                        </td>
                                        <td>
                                            <?php echo e(number_format($item->total_price)); ?>

                                        </td>
                                        <td>
                                            <?php echo e($item -> name); ?>

                                        </td>
                                        <td>
                                            <?php echo e($item->transaction_status); ?>

                                        </td>
                                        <td>
                                            

                                            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#upload<?php echo e($item->id); ?>">Upload Bukti</a>

                                            
                                        </td>
                                        
                                        <td>
                                            <form action="<?php echo e(route('ordershow',$item  -> id)); ?>" method="POST" enctype="multipart/form-data" >
                                            <?php echo csrf_field(); ?>
                                                <button type="submit" class="btn btn-primary"> Show </button>
                                            </form>
                                        </td>
                                    </tr> 
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Upload Bukti -->
    <?php $__currentLoopData = $history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $low): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="modal fade" tabindex="-1" id="upload<?php echo e($low->id); ?>">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Upload Bukti Pembayaran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">  
                        <div class="card bg-light mb-3" style="max-width: 30rem;">
                            <div class="card-header">Nomor Rekening : <strong>Mandiri</strong></div>
                            <div class="card-body">
                                <h5 class="card-title">A/N Salsabila Endang Saf</h5>
                                <p class="card-text">1270009890821.</p>
                            </div>
                        </div>
                        <div>
                            <h5>Keterangan :</h5>
                            <p class="text-danger">
                                -Upload bukti pembayaran lewat whatsapp <br>
                                -Tulisan Otomatis jangan ubah atau dihapus.
                            </p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a href="https://wa.me/6287881859294?text=Nama: <?php echo e($item->name); ?>, kode transaksi: <?php echo e($item->id); ?>, kode: <?php echo e($item->code); ?> " class="btn btn-primary">Upload</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <?php $__currentLoopData = $history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $low): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="modal fade" tabindex="-1" id="uploads<?php echo e($low->id); ?>">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Upload Bukti Pembayaran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form action="/uploadBukti" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                                <div class="form-group">
                                    <label>Kode : <strong><?php echo e($low->code); ?> :</strong></label>
                                    <label>Upload Bukti :</label>
                                    <input type="file" class="form-control-file" name="file">
                                    <input type="number" class="form-control" name="id_transaksi" value="<?php echo e($low->id); ?>" readonly>
                                    <input type="number" class="form-control" name="id_transaksi" value="<?php echo e(Auth::user()->id); ?>" readonly>
                                </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card bg-light mb-3" style="max-width: 30rem;">
                                <div class="card-header">Nomor Rekening : <strong>Mandiri</strong></div>
                                <div class="card-body">
                                    <h5 class="card-title">A/N Salsabila Endang Saf</h5>
                                    <p class="card-text">1270009890821.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('fixed','fixed-bottom'); ?>

<?php echo $__env->make('frontend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sweetbites2\resources\views/frontend/orderhistory.blade.php ENDPATH**/ ?>