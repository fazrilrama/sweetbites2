<?php $__env->startSection('content'); ?>

    <!-- Page Content -->
    <div class="page-content page-categories">
        <section class="store-trend-categories">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h5 class="text-center"> Category</h5>
                        <img src="<?php echo e(asset('frontend/images/logo1.png')); ?>" alt="">
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="100">
                        <a class="component-categories d-block" href="<?php echo e(route('ctigasapi')); ?>">
                            <div class="categories-image">
                                <img src="<?php echo e(asset('frontend/images/dessert.jpg')); ?>" alt="Gadgets Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Desserts
                            </p>
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="200">
                        <a class="component-categories d-block" href="<?php echo e(route('ctayam')); ?>">
                            <div class="categories-image">
                                <img src="<?php echo e(asset('frontend/images/cake.jpg')); ?>" alt="Furniture Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Cakes
                            </p>
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="300">
                        <a class="component-categories d-block" href="<?php echo e(route('cthas')); ?>">
                            <div class="categories-image">
                                <img src="<?php echo e(asset('frontend/images/brownies.jpg')); ?>" alt="Makeup Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Baked Brownies
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="store-new-products mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h5 class="text-center"> Our Desserts </h5>
                        <img src="<?php echo e(asset('frontend/images/logo2.png')); ?>" width="80px" height="57px" alt="">
                    </div>
                </div>
                <div class="row mt-5">
                    <?php $incrementProduct = 0 ?>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-6 col-md-4 col-lg-3" data-aos="fade-right"
                            data-aos-delay="<?php echo e($incrementProduct += 100); ?>">
                            <a class="component-products d-block" href="<?php echo e(route('detail', $item->id)); ?>">
                                <div class="products-thumbnail">
                                    <div class="products-image"
                                        style="background-image: url('<?php echo e(Storage::url($item->galleries->first()->Photos)); ?>');">
                                    </div>
                                </div>
                                <div class="products-text">
                                    <?php echo e($item->ProductName); ?>

                                </div>
                                <div class="products-price">
                                    Rp <?php echo e(number_format($item->Price)); ?>

                                </div>
                            </a>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </section>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\kuliah\PI\meatstorefix-main\resources\views/frontend/categories/ctigasapi.blade.php ENDPATH**/ ?>