<?php $__env->startSection('title'); ?>
    Store Cart Page
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- Page Content -->
    <!-- Page Content -->
    <div class="page-content page-cart">
        <section class="store-breadcrumbs" data-aos="fade-down" data-aos-delay="100">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    Cart
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        <?php endif; ?>
        <form action="<?php echo e(route('checkoutdata')); ?>" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
        <section class="store-cart">
            <div class="container">
                <div class="row" data-aos="fade-up" data-aos-delay="100">
                    <div class="col-12 table-responsive">
                        <table class="table table-borderless table-cart responsive" aria-describedby="Cart">
                            <thead>
                                <tr>
                                    <th scope="col">Image</th>
                                    <th scope="col">Name </th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Shipping</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Menu</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $totalPrice = 0 ?>
                                <?php $__currentLoopData = $carts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <td style="width: 20%;">
                                                    <?php if($cart->product->galleries): ?>
                                                    <img src="<?php echo e(asset('foto/' . $cart->product->galleries->first()->Photos)); ?>" alt="" class="cart-image" />
                                                    <?php endif; ?>
                                                </td>
                                            </div>
                                            <div class="col-md-2">
                                                <td>
                                                    <div class="product-title"><?php echo e($cart->product->ProductName); ?></div>
                                                </td>
                                            </div>
                                            <div class="col-md-2">
                                                <td>
                                                    <div class="product-title">Rp <?php echo e(number_format($cart->product->Price)); ?></div>
                                                </td>
                                            </div>
                                            <div class="col-md-2">
                                                <td>
                                                    <div class="product-title">Rp <strong><?php echo e(number_format(20000)); ?></strong></div>
                                                </td>
                                            </div>
                                            <div class="col-md-2">
                                                <td>
                                                    <input type="number" name="Quantity" style="width: 100px" class="form-control mt-3" placeholder="" value="1" required>
                                                </td>
                                            </div>
                                            <div class="col-md-2">
                                                <td>
                                                    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
                                                    <button class="btn btn-remove-cart" id="deleterecord" type="button" data-id="<?php echo e($cart->id); ?>">
                                                        Remove
                                                    </button>
                                                </td>
                                            </div>
                                        </div>
                                    </tr>
                                    <?php $totalPrice += $cart->product->Price ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" data-aos="fade-up" data-aos-delay="150">
                    <div class="col-12">
                        <hr />
                    </div>
                    <div class="col-12">
                        <h2 class="mb-4">Shipping Details</h2>
                    </div>
                </div>
               
                    <input type="hidden" name="total_price" value="<?php echo e($totalPrice); ?>">
                    <div class="row mb-2" data-aos="fade-up" data-aos-delay="200" id="locations">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address_one">Name </label>
                                <input type="text" class="form-control" name="name" readonly value="<?php echo e(Auth::user()->name); ?>" required/>
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="form-group">
                                <label for="address_one">Email </label>
                                <input type="text" class="form-control" name="email" readonly value="<?php echo e(Auth::user()->email); ?>" required/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address_one">Address 1</label>
                                <input type="text" class="form-control" name="address_one" value="" required/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address_two">Address 2</label>
                                <input type="text" class="form-control" id="address_two" name="address_two"
                                    value="" required/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="provinces_id">Province</label>
                                <select name="province" id="province" class="form-control">
                                    <option value="0" disabled="true" selected="true"> Select Province </option>
                                    <option> Jawa Barat </option>
                                    <option> DKI Jakarta</option>
                                   
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="provinces_id">City</label>
                                <select name="city" id="province" class="form-control">
                                    <option value="0" disabled="true" selected="true"> Select City </option>
                                    <option> Depok </option>
                                    <option> Jakarta Selatan </option>
                                    <option> Jakarta Timur </option>
                                    <option> Jakarta Pusat </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="zip_code">Postal Code</label>
                                <input type="text" class="form-control" id="zip_code" name="zip_code" required/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone_number">Mobile Phone</label>
                                <input type="text" class="form-control" id="phone_number" name="phone" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row" data-aos="fade-up" data-aos-delay="150">
                        <div class="col-12 col-md-4" data-aos="fade-up" data-aos-delay="200">
                            <button type="submit" class="btn btn-success mt-4 px-4 btn-block">
                                Checkout Now
                            </button>
                        </div>
                        <hr />
                    </div>
                </form>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
    $("#deleterecord").click(function() {
        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");

        $.ajax({
            url: "cart/" + id,
            type: 'DELETE',
            data: {
                "id": id,
                "_token": token,
            },
            success: function() {
                console.log("it Works");
            }
        });

        location.reload(true);

    });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sweetbites2\resources\views/frontend/cart.blade.php ENDPATH**/ ?>