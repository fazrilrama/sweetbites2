   <meta charset="utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
   <meta name="description" content="" />
   <meta name="author" content="" />
   <title>Sweetbites By Caca Web Store</title>
   <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
   <link href="<?php echo e(asset('frontend/style/main.css')); ?>" rel="stylesheet" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

   <?php echo $__env->yieldContent('style'); ?>
<?php /**PATH C:\kuliah\PI\meatstorefix-main\resources\views/frontend/include/head.blade.php ENDPATH**/ ?>