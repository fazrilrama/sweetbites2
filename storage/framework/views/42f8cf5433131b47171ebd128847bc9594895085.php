<?php $__env->startSection('title'); ?>
    Store Dashboard
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- Section Content -->
    <div class="section-content section-dashboard-home" >
        <div class="container-fluid">
            <div class="dashboard-heading">
                <h2 class="dashboard-title">Menu Gallery</h2>
                <p class="dashboard-subtitle">
                    List of Gallery
                </p>
            </div>
            <div class="dashboard-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="<?php echo e(route('product-galleries.create')); ?>" class="btn btn-primary mb-3">
                                    + Add New Gallery 
                                </a>
                                <div class="table-responsive">
                                    <table class="table table-hover scroll-horizontal-vertical w-100" id="crudTable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Menu</th>
                                                <th>Photos</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td>
                                                        <?php echo e($item->id); ?>

                                                    </td>
                                                    <td>
                                                        <?php echo e($item->product->ProductName); ?>

                                                    </td>
                                                    <td>
                                                        <img src="<?php echo e(Storage::url($item->Photos)); ?>" alt=""
                                                            class="img-thumbnail" width="100px" height="80px">
                                                    </td>
                                                    <td>
                                                            <form action="<?php echo e(route('product-galleries.destroy',$item->id)); ?>" method="POST" enctype="multipart/form-data" class="d-inline">
                                                            <?php echo csrf_field(); ?>
                                                            <?php echo method_field('DELETE'); ?>
                                                                <button type="submit" class="d-inline btn btn-warning"> Delete </button>
                                                            </form>
                                                    </td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startPush('addon-script'); ?>
    <script>
        // AJAX DataTable
        var datatable = $('#crudTable').DataTable({
            processing: true,
            serverSide: true,
            ordering: true,
            ajax: {
                url: '<?php echo url()->current(); ?>',
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'product.name',
                    name: 'product.name'
                },
                {
                    data: 'photos',
                    name: 'photos'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false,
                    width: '15%'
                },
            ]
        });

    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('backend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\kuliah\PI\meatstorefix-main\resources\views/backend/product-galleries/index.blade.php ENDPATH**/ ?>