<?php $__env->startSection('content'); ?>

    <!-- Page Content -->
    <div class="page-content page-categories">
        <section class="store-trend-categories">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h5 class="text-center"> About Us </h5>
                        <img src="<?php echo e(asset('frontend/images/logo1.png')); ?>" alt="">
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 col-sm-12 col-lg-6" data-aos="fade-up" data-aos-delay="400">
                        <a class="component-categories d-block" href="<?php echo e(route('ctigasapi')); ?>">
                            <div class="categories-image">
                                <img src="<?php echo e(asset('frontend/images/dessert.jpg')); ?>" alt="Gadgets Categories"
                                    class="w-100" height="400px" />
                            </div>
                            <p class="categories-text">
                                Regal Dessert Box
                            </p>
                        </a>
                    </div>
                    <div class="col-md-12  col-sm-12 col-lg-6" data-aos="fade-up" data-aos-delay="20">
                        <a class="component-categories d-block" href="<?php echo e(route('ctigasapi')); ?>">
                            <div class="categories-image" style="height: 455px">
                               <h5> Sweetbites by Caca merupakan sebuah home industry yang bergerak di bidang kuliner yang dikelola oleh seorang mahasiswi dari Universitas Gunadarma. Pada awalnya, owner hanya menjual satu macam saja yaitu regal dessert box. Namun karena banyaknya permintaan dari berbagai belah pihak akhirnya owner menambah menu pilihan lainnya. Kini Sweetbites By Caca tidak hanya menjual dessert box, namun juga ada brownies panggang dan milo jelly sagoo yang bisa kamu rasakan. So, what do you waiting for? Let's buy some! ^^ </h5>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('fixed','fixed-bottom'); ?>
    

<?php echo $__env->make('frontend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\kuliah\PI\meatstorefix-main\resources\views/frontend/about.blade.php ENDPATH**/ ?>