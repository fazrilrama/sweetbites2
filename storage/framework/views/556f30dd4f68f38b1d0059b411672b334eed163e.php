    <nav class="navbar navbar-expand-lg navbar-light navbar-store fixed-top navbar-fixed-top" data-aos="fade-down">
        <div class="container">
            <a class="navbar-brand" href="<?php echo e(route('home')); ?>">
                <img src="/images/logo1.png" alt="" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active mt-2">
                        <a class="nav-link" href="/">Home </a>
                    </li>
                    <li class="nav-item mt-2">
                        <a class="nav-link" href="<?php echo e(route('categories')); ?>">Category</a>
                    </li>
                    <li class="nav-item mt-2">
                        <a class="nav-link" href="<?php echo e(route('aboutme')); ?>">About</a>
                    </li>
                    <?php if(auth()->guard()->check()): ?>
                        <li class="nav-item mt-2">
                            <a class="nav-link" href="<?php echo e(route('orderhistory')); ?>">Order History</a>
                        </li>
                    <?php endif; ?>
                    <?php if(auth()->guard()->guest()): ?>
                        <li class="nav-item mt-2">
                            <a class="nav-link" href="<?php echo e(route('register')); ?>">Sign Up</a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="btn btn-success nav-link px-4 text-white" href="<?php echo e(route('login')); ?>">Login</a>
                        </li>
                    <?php endif; ?>
                    <?php if(auth()->guard()->check()): ?>
                        <!-- Desktop Menu -->
                        <li class="nav-item">
                            <a href="<?php echo e(route('cart.index')); ?>" class="nav-link d-inline-block mt-2">
                                <?php
                                    $carts = \App\Models\Cart::where('users_id', Auth::user()->id)->count();
                                ?>
                                <?php if($carts > 0): ?>
                                    <a class="nav-link d-inline-block mt-2" href="<?php echo e(route('cart.index')); ?>">
                                        <img src="<?php echo e(asset('frontend/images/icon-cart-filled.svg')); ?>" alt="" />
                                        <div class="cart-badge"><?php echo e($carts); ?></div>
                                    </a>
                                <?php else: ?>
                                    <img src="<?php echo e(asset('frontend/images/icon-cart-empty.svg')); ?>" alt="" />
                                <?php endif; ?>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <img src="<?php echo e(asset('frontend/images/logo2.png')); ?>" alt=""
                                    class="rounded-circle mr-2 profile-picture" />
                                Hi, <?php echo e(Auth::user()->name); ?>

                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <?php if(Auth::user()->roles == 'ADMIN'): ?>
                                <a class="dropdown-item" href="<?php echo e(route('dashboard')); ?>">Dashboard Admin</a>
                                <?php endif; ?>
                                <div class="dropdown-divider"></div>
                                <form action="<?php echo e(route('logout')); ?>" method="post">
                                <?php echo csrf_field(); ?>
                                    <button type="submit" class="dropdown-item""> Logout </button>
                                </form>
                            </div>
                        </li>
                    </ul>

                    <!-- Mobile Menu -->
                    <ul class="navbar-nav d-block d-lg-none">
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                Hi, <?php echo e(Auth::user()->name); ?>

                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-inline-block" href="#">
                                Cart
                            </a>
                        </li>
                    </ul>
                <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>
<?php /**PATH C:\kuliah\PI\sweetbites2\resources\views/frontend/include/navbar.blade.php ENDPATH**/ ?>