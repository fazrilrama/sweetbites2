<?php $__env->startSection('content'); ?>

    <div class="section-content section-dashboard-home">
        <div class="container-fluid">
            <div class="dashboard-heading">
                <h2 class="dashboard-title">Add New Menu</h2>
                <p class="dashboard-subtitle">
                    Create my own menu
                </p>
            </div>
            <div class="dashboard-content">
                <div class="row">
                    <div class="col-12">
                        <form action="<?php echo e(route('product.update',$data -> id)); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('PUT'); ?>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Menu Name</label>
                                                <input type="text" class="form-control" id="name" aria-describedby="name"
                                                    name="ProductName" value="<?php echo e($data -> ProductName); ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="price">Price</label>
                                                <input type="number" class="form-control" id="price"
                                                    aria-describedby="price" name="Price" value="<?php echo e($data -> Price); ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="categories">Category</label>
                                                <select class="form-control" id="category" name="Categories">
                                                    <option><?php echo e($data -> Categories); ?></option>
                                                    <?php if($data -> Categories == 'Cakes'): ?>
                                                    <option>Baked Brownies</option>
                                                    <option>Desserts</option>
                                                    <?php elseif($data -> Categories == 'Baked Brownies'): ?> 
                                                    <option>Cakes</option>
                                                    <option>Desserts</option>
                                                    <?php else: ?>
                                                    <option>Cakes</option>
                                                    <option>Baked Brownies</option>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="categories">Weight (Gr) </label>
                                                <input type="number" class="form-control" id="price"
                                                    aria-describedby="price" name="Weight" value="<?php echo e($data -> Weight); ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="description">Descriptions</label>
                                                <textarea name="Description" id="" cols="30" rows="4"
                                                    class="form-control"> <?php echo e($data -> Description); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="thumbnails">Thumbnails</label>
                                                <input type="file" multiple class="form-control pt-1" id="thumbnails"
                                                    aria-describedby="thumbnails" name="ThumbnailPhoto" />
                                                <small class="text-muted">
                                                    Kamu dapat memilih Gambar untuk thumbnail
                                                </small>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success btn-block px-5">
                                                    Save Now
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\kuliah\PI\meatstorefix-main\resources\views/backend/product/edit.blade.php ENDPATH**/ ?>