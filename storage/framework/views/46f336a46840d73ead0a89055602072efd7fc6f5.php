<?php $__env->startSection('title'); ?>
    Store Cart Page
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- Page Content -->
    <!-- Page Content -->
    <div class="page-content page-cart">
        <section class="store-breadcrumbs" data-aos="fade-down" data-aos-delay="100">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    Cart
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <section class="store-cart">
            <div class="container">
                <div class="row" data-aos="fade-up" data-aos-delay="100">
                    <div class="col-12 table-responsive">
                        <table class="table table-borderless table-cart" aria-describedby="Cart">
                            <thead>
                                <tr>
                                    <th scope="col">Image</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $totalPrice = 0 ?>
                                <?php $__currentLoopData = $carts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td style="width: 20%;">
                                            <?php if($cart->product->galleries): ?>
                                                <img src="<?php echo e(Storage::url($cart->product->galleries->first()->Photos)); ?>"
                                                    alt="" class="cart-image" />
                                            <?php endif; ?>
                                        </td>
                                        <td style="width: 35%;">
                                            <div class="product-title"><?php echo e($cart->product->ProductName); ?></div>
                                        </td>
                                        <td style="width: 35%;">
                                            <div class="product-title"><?php echo e(number_format($cart->product->Price)); ?></div>
                                            <div class="product-subtitle"></div>
                                        </td>
                                        <td style="width: 35%;">
                                            <div class="product-title"><?php echo e($cart -> Quantity); ?></div>
                                            <div class="product-subtitle"></div>
                                        </td>
                                    </tr>
                               
                                    <?php $totalPrice += $cart->product->Price ?>
                                    <?php $allprice = $totalPrice * $cart -> Quantity  ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" data-aos="fade-up" data-aos-delay="150">
                    <div class="col-12">
                        <hr />
                    </div>
                    <div class="col-12">
                        <h2 class="mb-4">Shipping Details</h2>
                    </div>
                </div>
                <form action="<?php echo e(route('checkout')); ?>" method="POST" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" class="form-control" name="name" value="<?php echo e($name); ?>" />
                    <input type="hidden" class="form-control" name="email" value="<?php echo e($email); ?> " />
                    <input type="hidden" class="form-control" name="address_one" value="<?php echo e($address_one); ?> " />
                    <input type="hidden" class="form-control" name="address_two" value="<?php echo e($address_two); ?> " />
                    <input type="hidden" class="form-control" name="province" value="<?php echo e($province); ?> " />
                    <input type="hidden" class="form-control" name="city" value="<?php echo e($city); ?> " />
                    <input type="hidden" class="form-control" name="zip_code" value="<?php echo e($zip_code); ?> " />
                    <input type="hidden" class="form-control" name="phone" value="<?php echo e($phone); ?> " />
                    <input type="hidden" class="form-control" name="total_price" value="<?php echo e($allprice); ?> " />
                    <div class="row mb-2" data-aos="fade-up" data-aos-delay="200" id="locations">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address_one">Name </label>
                                <h5> <?php echo e($name); ?>

                                </h5>
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="form-group">
                                <label for="address_one">Mail </label>
                                <h5> <?php echo e($email); ?>

                                </h5>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address_one">Address 1</label>
                                <h5> <?php echo e($address_one); ?>

                                </h5>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address_two">Address 2</label>
                                <h5> <?php echo e($address_two); ?>

                                </h5>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="provinces_id">Province</label>
                                <h5> <?php echo e($province); ?>

                                </h5>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="city">City</label>
                                <h5> <?php echo e($city); ?>

                                </h5>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="zip_code">Zip Code</label>
                                <h5> <?php echo e($zip_code); ?>

                                </h5>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone_number">Mobile Phone</label>
                                <h5> <?php echo e($phone); ?>

                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="row" data-aos="fade-up" data-aos-delay="150">
                        <div class="col-12">
                            <hr />
                        </div>
                        <div class="col-12">
                            <h2 class="mb-1">Payment Informations</h2>
                        </div>
                    </div>
                    <div class="row" data-aos="fade-up" data-aos-delay="200">
                        <div class="col-4 col-md-2">
                            <div class="product-title"> <img src="<?php echo e(asset('frontend/images/ic_bank.png')); ?>" alt="">
                            </div>
                            <div class="product-subtitle">Transfer Bank</div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="product-title text-success"><?php echo e(number_format($totalPrice * $cart -> Quantity)); ?></div>
                            <div class="product-subtitle">Total</div>
                        </div>
                        <div class="col-12 col-md-6">
                            <button type="submit" class="btn btn-success mt-4 px-4 btn-block">
                                Continue Payment
                            </button>
                        </div>
                        <div class="col-12 col-md-6">
                            <a href="<?php echo e(route('cart.index')); ?>" class="btn btn-success mt-4 px-4 btn-block text-white">
                                Cancel 
                            </a>
                        </div>
                    </div>
                </form>
              
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('frontend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\SALSA\Downloads\meatstorefix-main\resources\views/frontend/paymentorder.blade.php ENDPATH**/ ?>