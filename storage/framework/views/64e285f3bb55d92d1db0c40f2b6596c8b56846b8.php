

<?php $__env->startSection('content'); ?>
<!-- Page Content -->
<div class="page-content page-categories">
    <h3 class="container-fluid mr-2 text-center"> Order History </h3>
    <div class="card">
        <div class="col-12 mt-2">
            <div class="card-body">
                <table class="table">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" class="pt-4 pb-4">Code Order</th>
                            <th scope="col" class="pt-4 pb-4">Total Price</th>
                            <th scope="col" class="pt-4 pb-4">Name</th>
                            <th scope="col" class="pt-4 pb-4">Status</th>
                            <th scope="col" class="pt-4 pb-4">Payment</th>
                            <th scope="col" class="pt-4 pb-4">Action</th>
                        </tr>
                    </thead>
                    
                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('fixed','fixed-bottom'); ?>






<?php echo $__env->make('frontend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sweetbites2\resources\views/order.blade.php ENDPATH**/ ?>