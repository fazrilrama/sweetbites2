<?php $__env->startSection('content'); ?>

<div class="section-content section-dashboard-home" >
    <div class="container-fluid">
        <div class="dashboard-heading">
            <h2 class="dashboard-title">Dashboard</h2>
            <p class="dashboard-subtitle">
                Transaction Order 
            </p>
        </div>
        <div class="card">
            <div class="col-12 mt-2">
                    <div class="card-body">
                        <table class="table">
                            <thead class="thead-light">
                              <tr>
                                <th scope="col" class="pt-4 pb-4">Code Order</th>
                                <th scope="col" class="pt-4 pb-4">Total Price</th>
                                <th scope="col" class="pt-4 pb-4">Name</th>
                                <th scope="col" class="pt-4 pb-4">Status</th>
                                <th scope="col" class="pt-4 pb-4">Payment</th>
                                <th scope="col" class="pt-4 pb-4">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td>
                                       <h4> <?php echo e($item  -> code); ?> </h4>
                                       <p> <?php echo e($item  -> created_at); ?></p>
                                    </td>
                                    <td>
                                        <?php echo e($item -> total_price); ?>

                                    </td>
                                    <td>
                                        <?php echo e($item -> name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($item  -> province); ?>

                                    </td>
                                    <td>
                                        <?php echo e($item  -> transaction_status); ?>

                                    </td>
                                    
                                    <td>
                                        <a href="<?php echo e(route('transaction.show',$item->id)); ?>" class="btn btn-primary"> Show </a>
                                        <a href="<?php echo e(route('transaction.edit',$item->id)); ?>" class="btn btn-primary"> Edit </a>
                                    </td>
                                </tr> 
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                          </table>
                    </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\kuliah\PI\sweetbites2\resources\views/backend/transaction/index.blade.php ENDPATH**/ ?>