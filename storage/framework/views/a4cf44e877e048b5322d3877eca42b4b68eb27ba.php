<?php $__env->startSection('content'); ?>


    <div class="section-content section-dashboard-home">
        <div class="container-fluid">
            <div class="dashboard-heading">
                <h2 class="dashboard-title">My Menu</h2>
                <p class="dashboard-subtitle">
                    Manage it well and get money
                </p>
            </div>
            <div class="dashboard-content">
                <div class="row">
                    <div class="col-12">
                        <a href="<?php echo e(route('product.create')); ?>" class="btn btn-success">Add New Menu</a>
                    </div>
                </div>
                <div class="row mt-4">
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <a class="card card-dashboard-product d-block" href="<?php echo e(route('detail', $item->id)); ?>">
                                <div class="card-body">
                                    <?php if(!$item->galleries->isEmpty()): ?>
                                        <img src="<?php echo e(Storage::url($item->ThumbnailPhoto)); ?>" alt=""
                                            height="200px" class="w-100 mb-2" />
                                    <?php else: ?>
                                        <img src="" alt="" height="140px" class="w-100 mb-2" />
                                    <?php endif; ?>
                                    <div class="product-title"><?php echo e($item->ProductName); ?></div>
                                    <div class="product-category"><?php echo e($item->Categories); ?></div>
                                </div>
                                <a href="<?php echo e(route('product.edit',$item->id)); ?>" class="btn btn-info"> Edit </a>
                                <form action="<?php echo e(route('product.destroy',$item -> id)); ?>" method="POST" class="d-inline">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('DELETE'); ?>
                                <button type="submit" class="btn btn-warning"> Delete </button>
                                </form>
                            </a>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\kuliah\PI\sweetbites2\resources\views/backend/product/index.blade.php ENDPATH**/ ?>