<?php $__env->startSection('content'); ?>

    <!-- Page Content -->

    <div class="page-content page-categories">  
    <h3 class="container-fluid mr-2 text-center"> Order History </h3>
        <div class="card">
            <div class="col-12 mt-2">
                    <div class="card-body">
                        <table class="table">
                            <thead class="thead-light">
                              <tr>
                                <th scope="col" class="pt-4 pb-4">Code Order</th>
                                <th scope="col" class="pt-4 pb-4">Total Price</th>
                                <th scope="col" class="pt-4 pb-4">Name</th>
                                <th scope="col" class="pt-4 pb-4">Status</th>
                                <th scope="col" class="pt-4 pb-4">Payment</th>
                                <th scope="col" class="pt-4 pb-4">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td>
                                       <h4> <?php echo e($item  -> code); ?> </h4>
                                       <p> <?php echo e($item  -> created_at); ?></p>
                                    </td>
                                    <td>
                                        <?php echo e($item -> total_price); ?>

                                    </td>
                                    <td>
                                        <?php echo e($item -> name); ?>

                                    </td>
                                    <td>
                                        <?php echo e($item  -> province); ?>

                                    </td>
                                    <td>
                                        <?php echo e($item  -> transaction_status); ?>

                                    </td>
                                    
                                    <td>
                                        <form action="<?php echo e(route('ordershow',$item  -> id)); ?>" method="POST" enctype="multipart/form-data" >
                                        <?php echo csrf_field(); ?>
                                            <button type="submit" class="btn btn-primary"> Show </button>
                                        </form>
                                    </td>
                                </tr> 
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                          </table>
                    </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('fixed','fixed-bottom'); ?>

<?php echo $__env->make('frontend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\kuliah\PI\meatstorefix-main\resources\views/frontend/orderhistory.blade.php ENDPATH**/ ?>