<!DOCTYPE html>
<html lang="en">

<head>
    <?php echo $__env->make('frontend.include.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</head>

<body>
    <div class="page-dashboard">
        <div class="d-flex" id="wrapper">
            <?php echo $__env->make('backend.include.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div id="page-content-wrapper">
                <div id="page-content-wrapper">
                    <nav class="navbar navbar-store navbar-expand-lg navbar-light fixed-top">
                        <button class="btn btn-secondary d-md-none mr-auto mr-2" id="menu-toggle">
                            &laquo; Menu
                        </button>

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto d-none d-lg-flex">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img src="<?php echo e(asset('frontend/images/logo2.png')); ?>" alt="" class="rounded-circle mr-2 profile-picture" />
                                        Hi, <?php echo e(Auth::user()->name); ?>

                                        <form action="<?php echo e(route('logout')); ?>" class="d-inline" method="POST">
                                            <?php echo csrf_field(); ?>
                                            <button type="submit" class="d-inline btn btn-info"> Logout </button>
                                        </form>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                        <a class="dropdown-item" href="/index.html">Back to Store</a>
                                        <a class="dropdown-item" href="/dashboard-account.html">Settings</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="/">Logout</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-inline-block mt-2" href="">
                                        <img src="/images/icon-cart-empty.svg" alt="" />
                                    </a>
                                </li>
                            </ul>
                            <!-- Mobile Menu -->
                            <ul class="navbar-nav d-block d-lg-none mt-3">
                                <li class="nav-item">
                                    <a class="nav-link" href="#">
                                        Hi, <?php echo e(Auth::user()->name); ?>

                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-inline-block" href="#">
                                        Cart
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <?php echo $__env->yieldContent('content'); ?>
                </div>
            </div>
        </div>
    </div>

    <?php echo $__env->make('frontend.include.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar', 
            data: {
                labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
                , datasets: [{
                    label: '# Grafik pendapatan perbulan'
                    , data: [<?php echo json_encode($januari); ?>, <?php echo json_encode($februari); ?>, <?php echo json_encode($maret); ?>, <?php echo json_encode($april); ?>, <?php echo json_encode($mei); ?>, <?php echo json_encode($juni); ?>, <?php echo json_encode($juli); ?>, <?php echo json_encode($agustus); ?>, <?php echo json_encode($september); ?>, <?php echo json_encode($oktober); ?>, <?php echo json_encode($november); ?>, <?php echo json_encode($desember); ?>]
                    , backgroundColor: [
                        'rgba(255, 99, 132, 0.2)'
                        , 'rgba(54, 162, 235, 0.2)'
                        , 'rgba(255, 206, 86, 0.2)'
                        , 'rgba(75, 192, 192, 0.2)'
                        , 'rgba(153, 102, 255, 0.2)'
                        , 'rgba(255, 159, 64, 0.2)'
                    ]
                    , borderColor: [
                        'rgba(255, 99, 132, 1)'
                        , 'rgba(54, 162, 235, 1)'
                        , 'rgba(255, 206, 86, 1)'
                        , 'rgba(75, 192, 192, 1)'
                        , 'rgba(153, 102, 255, 1)'
                        , 'rgba(255, 159, 64, 1)'
                    ]
                    , borderWidth: 1
                }]
            }
            , options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>


</body>

</html>
<?php /**PATH C:\xampp\htdocs\sweetbites2\resources\views/backend/include/appGrafik.blade.php ENDPATH**/ ?>