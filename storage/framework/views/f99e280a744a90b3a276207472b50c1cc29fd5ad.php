<?php $__env->startSection('content'); ?>

<div class="section-content section-dashboard-home" >
    <div class="container-fluid">
        <div class="dashboard-heading">
            <h2 class="dashboard-title">Dashboard</h2>
            <p class="dashboard-subtitle">
                Transaction Order 
            </p>
        </div>
            <div class="card" >
                <div class="col-12 mt-2">
                        <div class="card-body">
                        <h4 class="font-weight-bold"> Order ID/<?php echo e($history-> code); ?>/<?php echo e($history -> transaction_status); ?>/ <?php echo e($history -> created_at); ?> </h4>
                        <hr>
                        <div class="row mt-5">
                        <div class="col-md-6">
                            <h4 class="font-weight-bold"> Billing Address </h4>
                            <p class="mt-4"> <?php echo e($history -> name); ?> <br>
                                <?php echo e($history -> email); ?> <br> 
                                <br>
                                <?php echo e($history -> address_one); ?> <br> 
                                <?php echo e($history -> address_two); ?> <br> 
                                <?php echo e($history -> phone); ?> <br> 
                                <?php echo e($history -> province); ?> <br> 
                                <?php echo e($history -> city); ?> <br> 
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h4 class="font-weight-bold"> DETAILS</h4>
                            <p class="mt-4"> <?php echo e($history -> code); ?> <br>
                                <?php echo e($history -> created_at); ?> <br> 
                                Status : <?php echo e($history -> transaction_status); ?> <br> 
                                <?php echo e($history -> zip_code); ?> <br> 
                                <?php echo e($history -> phone); ?> <br> 
                            </p>
                        </div>
                        </div>
                </div>
            </div>

            <hr>
            <div class="col-12 mt-5">
                <table class="table responsive">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Categories</th>
                        <th scope="col">Weight</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Description</th>
                        <th scope="col">Price</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td>
                                <?php echo e($loop -> iteration); ?>

                            </td>
                            <td>
                                <?php echo e($item  -> product -> ProductName); ?>

                            </td>
                            <td>
                                <img width="100px" height="80px" class="img-thumbnail" src="<?php echo e(Storage::url($item  -> product -> ThumbnailPhoto )); ?>" alt="">
                            </td>
                            <td>
                                <?php echo e($item  -> product -> Categories); ?>

                            </td>
                            <td>
                                <?php echo e($item  -> product -> Weight); ?>

                        </td>
                        <td>
                            <?php echo e($item  -> quantity); ?>

                        </td>
                            <td>
                                <?php echo e($item  -> product -> Description); ?>

                            </td>
                            <td>
                                <?php echo e($item  -> product -> Price); ?>

                        </td>
                        </tr> 
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
            <div class="container col-12 mb-5">
                <div class="row">
                    <div class="col-lg-8">

                    </div>
                    <div class="col-lg-4">
                        <div class="row">

                        <div class="col-md-6">
                            <h4> Total Price </h4>
                        </div>
                        <div class="col-md-6">
                            <h4> <?php echo e($history -> total_price); ?> </h4> 
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sweetbites2\resources\views/backend/transaction/show.blade.php ENDPATH**/ ?>