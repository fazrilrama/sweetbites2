<?php $__env->startSection('content'); ?>
    <div class="page-content page-home">
        <section class="store-carousel">
            <div class="container">
            <?php if(session('message')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Selamat!</strong> <?php echo e(session('message')); ?>

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php endif; ?>
            <?php if(session('error')): ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Peringatan!</strong> <?php echo e(session('error')); ?>

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php endif; ?>
                <div class="row">
                    <div class="col-lg-12" data-aos="zoom-in">
                        <div id="storeCarousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#storeCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#storeCarousel" data-slide-to="1"></li>
                                <li data-target="#storeCarousel" data-slide-to="2"></li>
                                <li data-target="#storeCarousel" data-slide-to="3"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="<?php echo e(asset('frontend/images/banner1.jpg')); ?>" height="600px"
                                        style="border-radius: 20px" class="d-block w-100 " alt="Carousel Image" />
                                </div>
                                <div class="carousel-item">
                                    <img src="<?php echo e(asset('frontend/images/banner2.jpg')); ?>" class="d-block w-100 "
                                        style="border-radius: 20px" height="600px" alt="Carousel Image" />
                                </div>
                                <div class="carousel-item">
                                    <img src="<?php echo e(asset('frontend/images/banner3.jpg')); ?>" class="d-block w-100 "
                                        style="border-radius: 20px" height="600px" alt="Carousel Image" />
                                </div>
                                <div class="carousel-item">
                                    <img src="<?php echo e(asset('frontend/images/banner4.jpg')); ?>" class="d-block w-100 "
                                        style="border-radius: 20px" height="600px" alt="Carousel Image" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="store-trend-categories">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h5 class="text-center"> Category</h5>
                        <img src="<?php echo e(asset('frontend/images/logo1.png')); ?>" alt="">
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="200">
                        <div class="component-categories d-block">
                            <div class="categories-image">
                                <img src="<?php echo e(asset('frontend/images/dessert.jpg')); ?>" alt="Gadgets Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Dessert Box
                            </p>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="600">
                        <div class="component-categories d-block">
                            <div class="categories-image">
                                <img src="<?php echo e(asset('frontend/images/milo.jpg')); ?>" alt="Furniture Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Dessert Cup
                            </p>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-4" data-aos="fade-up" data-aos-delay="400">
                        <div class="component-categories d-block">
                            <div class="categories-image">
                                <img src="<?php echo e(asset('frontend/images/brownies.jpg')); ?>" alt="Makeup Categories"
                                    class="w-100" height="217px" />
                            </div>
                            <p class="categories-text">
                                Baked Brownies
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="store-trend-categories">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h5 class="text-center"> Suggest Menu</h5>
                        <img src="<?php echo e(asset('frontend/images/logo2.png')); ?>" alt="">
                    </div>
                </div>
                <div class="row mt-4 ">
                    <?php $incrementProduct = 0 ?>
                    <?php $__currentLoopData = $suggestproduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-6 col-md-3 col-lg-4 pt-3" data-aos="fade-up"
                            data-aos-delay="<?php echo e($incrementProduct += 100); ?>">
                            <a class=" d-block" style="text-decoration:none" href="<?php echo e(route('detail', $item->id)); ?>">
                                <p class="categories-text ">
                                    <h4 class="text-center"> <?php echo e($item->ProductName); ?> </h4>
                                </p>
                                <div class="categories-image">
                                    <img src="<?php echo e(asset('foto/' . $item->galleries->first()->Photos)); ?>" alt="Gadgets Categories" class="w-100" height="217px" />


                                    
                                </div>
                                <p class="categories-text ">
                                    <h6 class="text-center"> <?php echo e($item->Categories); ?> </h6>
                                </p>
                            </a>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </section>

        <section class="store-new-products mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" data-aos="fade-up">
                        <h5 class="text-center">New Menu </h5>
                        <img src="<?php echo e(asset('frontend/images/logo2.png')); ?>" width="80px" height="57px" alt="">
                    </div>
                </div>
                <div class="row mt-5">
                    <?php $incrementProduct = 0 ?>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-6 col-md-4 col-lg-3" data-aos="fade-right"
                            data-aos-delay="<?php echo e($incrementProduct += 100); ?>">
                            <a class="component-products d-block" href="<?php echo e(route('detail', $item->id)); ?>">
                                <div class="products-thumbnail">
                                    <div class="products-image"
                                        style="background-image: url('<?php echo e(asset('foto/' . $item->galleries->first()->Photos)); ?>');">


                                    </div>
                                </div>
                                <div class="products-text">
                                    <?php echo e($item->ProductName); ?>

                                </div>
                                <div class="products-price">
                                     Rp <?php echo e(number_format($item->Price)); ?>

                                </div>
                            </a>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.include.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sweetbites2\resources\views/frontend/LandingPage.blade.php ENDPATH**/ ?>