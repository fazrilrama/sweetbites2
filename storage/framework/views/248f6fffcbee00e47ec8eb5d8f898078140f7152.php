    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo e(asset('frontend/vendor/jquery/jquery.slim.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/vendor/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        AOS.init();

    </script>
    <script src="<?php echo e(asset('frontend/script/navbar-scroll.js')); ?>"></script>
    <?php echo $__env->yieldContent('script'); ?>

<?php /**PATH C:\kuliah\PI\sweetbites2\resources\views/frontend/include/script.blade.php ENDPATH**/ ?>